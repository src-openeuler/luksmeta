Name:           luksmeta
Version:        9
Release:        5
Summary:        LUKSMeta is a simple library for storing metadata in the LUKSv1 header
License:        LGPLv2+
URL:            https://github.com/latchset/%{name}
Source0:        https://github.com/latchset/%{name}/releases/download/v%{version}/%{name}-%{version}.tar.bz2

Patch1:         0001-Relax-content-tests-in-test-suite.patch
Patch2:         0002-Force-creation-of-LUKS1-containers-in-test-suite.patch
Patch3:         0003-Define-log-callback-function-to-use-with-libcryptset.patch

BuildRequires:  gcc asciidoc pkgconfig
BuildRequires:  cryptsetup-devel 

Provides:       lib%{name}
Obsoletes:      lib%{name}

%description
LUKSMeta is a simple library for storing metadata in the LUKSv1 header. Some projects need
to store additional metadata about a LUKS volume that is accessable before unlocking it.
Fortunately, there is a gap in the LUKS header between the end of the slot area and the
payload offset, LUKSMeta uses this hole to store additional metadata.

%package        devel
Summary:        Header files and other developing files
Requires:       %{name} = %{version}-%{release}

Provides:       lib%{name}-devel
Obsoletes:      lib%{name}-devel

%description    devel
This package contained all files for development.

%package        help
Summary:        Documents for %{name}
Buildarch:      noarch
Requires:       man info

%description    help
Man pages and other related documents for %{name}.

%prep
%autosetup -n %{name}-%{version} -p1

%build
%configure
%make_build

%install
%make_install

%check
make %{?_smp_mflags} check

%post -n %{name} -p /sbin/ldconfig
%postun -n %{name} -p /sbin/ldconfig

%files
%exclude %{_libdir}/libluksmeta.la
%license COPYING
%{_bindir}/luksmeta
%{_libdir}/libluksmeta.so.*

%files devel
%{_includedir}/luksmeta.h
%{_libdir}/libluksmeta.so
%{_libdir}/pkgconfig/luksmeta.pc

%files help
%{_mandir}/man8/luksmeta.8*


%changelog
* Fri Jul 30 2021 chenyanpanHW <chenyanpan@huawei.com> - 9-5
- DESC: delete -Sgit from %autosetup, and delete BuildRequires git

* Mon Jul 13 2020 Zhiqiang Liu <liuzhiqiang26@huawei.com> - 9-4
- backport upstream bugfix patches

* Fri Aug 30 2019 guiyao<guiyao@huawei.com> - 9-3
- Package init
